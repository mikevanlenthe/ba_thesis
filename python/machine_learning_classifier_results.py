from machine_learning import *

class VoteClassifier(ClassifierI):
    """ This class including its functions reads all the classifiers
        specified, generates the mode of classification and generates a confidence score
        based on how many classifier agreed on classification of the feature """
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        """ This function returns the mode of the predictions of the
            classifiers """
        votes = []
        for classifier in self._classifiers:
            vote = classifier.classify(features)
            votes.append(vote)
        return mode(votes)

    def confidence(self, features):
        """ This function returns a confidence score based on how many
            classifiers agreed on classification of the feature """
        votes = []
        for classifier in self._classifiers:
            vote = classifier.classify(features)
            votes.append(vote)

        choice_votes = votes.count(mode(votes))
        confidence_score = choice_votes / len(votes)
        return confidence_score

print("Collecting classifier results....")
# other classifiers
classifier = nltk.NaiveBayesClassifier.train(training_set)
MNB_classifier = SklearnClassifier(MultinomialNB())
MNB_classifier.train(training_set)
BernoulliNB_classifier = SklearnClassifier(BernoulliNB())
BernoulliNB_classifier.train(training_set)
LogisticRegression_classifier = SklearnClassifier(LogisticRegression())
LogisticRegression_classifier.train(training_set)
SGDClassifier = SklearnClassifier(SGDClassifier())
SGDClassifier.train(training_set)
SVC_classifier = SklearnClassifier(SVC())
SVC_classifier.train(training_set)
LinearSVC_classifier = SklearnClassifier(LinearSVC())
LinearSVC_classifier.train(training_set)
NuSVC_classifier = SklearnClassifier(NuSVC())
NuSVC_classifier.train(training_set)

# voting classifier
voted_classifier = VoteClassifier(classifier,
                                  NuSVC_classifier,
                                  LinearSVC_classifier,
                                  SGDClassifier,
                                  MNB_classifier,
                                  BernoulliNB_classifier,
                                  LogisticRegression_classifier)

# create list to loop for each classifier
classifiers_list = \
    [classifier, MNB_classifier, BernoulliNB_classifier, LogisticRegression_classifier,
    SGDClassifier, SVC_classifier, LinearSVC_classifier, NuSVC_classifier, voted_classifier]

classifiers_name_list = \
    ["NaiveBayesClassifier", "MNB_classifier", "BernoulliNB_classifier", "LogisticRegression_classifier",
    "SGDClassifier", "SVC_classifier", "LinearSVC_classifier", "NuSVC_classifier", "voted_classifier"]


# show most informative features of NaiveBayesClassifier
classifier.show_most_informative_features(20)

print()
input("Showing most informative features. Press <enter> to continue collecting results:")

######################################################
# print(classification_report(dev_and_test_set, classifier))
# print(confusion_matrix(dev_and_test_set, classifier))

# results from other classifiers on gold set
for clfr, clfr_name in zip(classifiers_list, classifiers_name_list):
    dev_test_accuracy = str(round(nltk.classify.accuracy(clfr, test_set), 4) * 100) + "%"

    # precision, recall and f-score of NaiveBayesClassifier on gold set
    refsets = collections.defaultdict(set)
    testsets = collections.defaultdict(set)
    for i, (feats, label) in enumerate(test_set):
        refsets[label].add(i)
        observed = clfr.classify(feats)
        testsets[observed].add(i)

    precision_positive = precision(refsets['positive'], testsets['positive'])
    recall_positive = recall(refsets['positive'], testsets['positive'])
    f_measure_positive = f_measure(refsets['positive'], testsets['positive'])
    precision_negative = precision(refsets['negative'], testsets['negative'])
    recall_negative = recall(refsets['negative'], testsets['negative'])
    f_measure_negative = f_measure(refsets['negative'], testsets['negative'])

    # metrics variable
    metrics = [
        (clfr_name, dev_test_accuracy, precision_positive, recall_positive,
        f_measure_positive, precision_negative, recall_negative, f_measure_negative)]

    print()

    # metrics sheet
    sheet = pe.Sheet(metrics)

    sheet.colnames = \
         [clfr_name, "Accuracy development set", "Precision pos", "Recall pos", "F-measure pos",
         "Precision neg", "Recall neg", "F-measure neg"]

    print(sheet)

    # save metrics
    sheet.save_as("results/classifier_results/test_results_{0}_classifier.xls".format(clfr_name))
