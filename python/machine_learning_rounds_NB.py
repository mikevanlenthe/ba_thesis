from machine_learning import *

# create and train NaiveBayes classifier
classifier = nltk.NaiveBayesClassifier.train(training_set)

# create folder list where to predict on
data_folders = \
    ['data/liveshow1', 'data/liveshow2', 'data/liveshow3', 'data/liveshow4', 'data/liveshow5', 'data/finale']

# create candidate list of tvoh participants
candidates_list = \
    [
    ["leon", "katell", "thijs", "vinchenzo", "dwight", "roza", "pleun", "romy", "yerry", "kirsten", "isabel", "sheela"],
    ["leon", "katell", "thijs", "vinchenzo", "dwight", "pleun", "romy", "yerry", "kirsten", "isabel"],
    ["leon", "thijs", "vinchenzo", "dwight", "pleun", "yerry", "kirsten", "isabel"],
    ["leon", "thijs", "vinchenzo", "dwight", "pleun", "yerry", "isabel"],
    ["leon", "thijs", "vinchenzo", "dwight", "pleun", "isabel"],
    ["thijs", "pleun", "vinchenzo", "isabel"]
    ]

# create jury list of each jury member
jury = ["ali", "waylon", "guus", "sanne"]

for d_folder, candidates in zip(data_folders, candidates_list):
    # data collection of unseen tweets
    unseen_tweets = \
        retrieve_tweets(d_folder)

    print("Predictions on {0} data by NaiveBayesClassifier:".format(d_folder.split("/")[1]))

    # collect sentiment score for each candidate *** modulair by list entry ***
    sentiment_scores = []

    for candidate in candidates:
        positive_counter, negative_counter, tweet_count = 0, 0, 0  # setup counters
        for tweet in unseen_tweets:  # set to unseen_tweets to predict the next round
            if candidate in tweet:
                prediction = classifier.prob_classify(extract_features(tknzr.tokenize(tweet)))
                # sentiment rating for tweet must be atleast 70% positive
                if prediction.max() == "positive" and prediction.prob("positive") >= .7:
                    # increase counters by 1 if positive tweet found about candidate
                    positive_counter += 1
                    tweet_count += 1

                # sentiment rating for tweet must be atleast 70% negative
                if prediction.max() == "negative" and prediction.prob("negative") >= .7:
                    # increase counters by 1 if negative tweet found about candidate
                    negative_counter += 1
                    tweet_count += 1

        sentiment_score = positive_counter - negative_counter
        sentiment_percentage = round((sentiment_score / tweet_count) * 100, 1)

        tuple = (
            candidate, tweet_count, positive_counter, negative_counter, sentiment_score,
             sentiment_percentage
            )  # set results of candidate into tuple format

        # append candidate scores to sentiment_scores list
        sentiment_scores.append(tuple)

    print()
    print("On name:")
    sentiment_scores.sort(key=itemgetter(0))

    # uncomment to print results
    # score_counter = 1
    # for score in sentiment_scores:
    #     print(
    #         "Nr: {6}, {0}: totaal aantal tweets: {1}, aantal positief: {2}, "
    #         "aantal negatief: {3}, sentiment score: {4}, percentage: {5}%"
    #         .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))
    #
    #     score_counter += 1  # increase score_counter

    # sheet ordered by name
    sheet = pe.Sheet(sentiment_scores)
    sheet.colnames = "Naam", "Tweets totaal", "Tweets positief", "Tweets negatief", "S-score", "S-percentage"
    print(sheet)
    sheet.save_as("results/prediction_results/NaiveBayes/{0}_dataset_results_by_name_by_NaiveBayes.xls".format(d_folder.split("/")[1]))

    ######################################################

    print()
    print("On score:")
    sentiment_scores.sort(key=itemgetter(4), reverse=True)

    # sheet ordered by score
    sheet = pe.Sheet(sentiment_scores)
    sheet.colnames = "Naam", "Tweets totaal", "Tweets positief", "Tweets negatief", "S-score", "S-percentage"
    sheet.save_as("results/prediction_results/NaiveBayes/{0}_dataset_results_by_score_by_NaiveBayes.xls".format(d_folder.split("/")[1]))
    print(sheet)

    ######################################################

    print()
    print("On percentage:")
    sentiment_scores.sort(key=itemgetter(5), reverse=True)

    # sheet ordered by percentage
    sheet = pe.Sheet(sentiment_scores)
    sheet.colnames = "Naam", "Tweets totaal", "Tweets positief", "Tweets negatief", "S-score", "S-percentage"
    print(sheet)
    sheet.save_as("results/prediction_results/NaiveBayes/{0}_dataset_results_by_percentage_by_NaiveBayes.xls".format(d_folder.split("/")[1]))

    print()  # print new line between predictions on data sets
