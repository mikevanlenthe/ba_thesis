import json
from os import listdir
import os.path
from nltk import word_tokenize
from nltk.tokenize.casual import (TweetTokenizer, casual_tokenize)
from nltk.util import ngrams
import re
from itertools import groupby, chain, islice


def remove_urls(text):
    """This method takes a string as argument and returns string without urls"""
    regex = re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', re.DOTALL)
    urls = re.findall(regex, text)
    for url in urls:
        text = text.replace(url[0], ', <URL>')
    return text


def extract_tweets(infile):
    """ takes file, reads file, returns list of tuples
        with text of tweet and username """
    tweets = []
    for line in open(infile, 'r'):
        if 'retweeted_status' in line:
            tweets.append((json.loads(line)['text'],
                json.loads(line)['retweeted_status']['user']['screen_name']))
        else:
            tweets.append((json.loads(line)['text'],
                json.loads(line)['user']['screen_name']))

    return tweets


def find_out_files(directory):
    """ This method takes a directory as argument,
        we can iterate over directory items (files to be processed),
        returns files list """
    files = []
    for filename in listdir(directory):
        if filename.endswith(".out"):
            files.append(directory + "/" + filename)
    return files


def find_text_files(directory):
    """ This method takes a directory as argument,
        we can iterate over directory items (files to be processed),
        returns files list """
    textfiles = []
    for filename in listdir(directory):
        if filename.endswith(".txt"):
            textfiles.append(directory + "/" + filename)
    return textfiles


def find_amount_text_files(directory):
    """ This method takes a directory as argument,
        we can iterate over directory items (files to be processed),
        returns amount of files """
    counter = 0
    for filename in listdir(directory):
        if filename.endswith(".txt"):
            counter = counter + 1

    return counter


def annotate_collected_tweets(tweet_tuples):
    """ This function reads a list of tuples with tweets and
        their file names and returns the (pre-pre processed) tweets of
        the files which then could be classified into positive
        or negative by user input and stores the annotated tweets
        into seperate corresponding directory with unique file name for
        identification / traceablilty """
    number = 1  # set starting number for tweet identification
    tknzr = TweetTokenizer()  # construct tokenizer for tweets
    stored_name_of_file = ""  # create variable for check if tweets comes from same .out file

    for tweet in tweet_tuples:  # loop trought each tweet in JSON .out file

        # set name of tweet file
        name_of_file = str(tweet[1][5:-4]) + "-tweet-" + str(number)  # set name of file as tweet + number for unique identification
        name_of_file_no_number = str(tweet[1][5:-4]) + "-tweet-"  # store name for check if tweets comes from same .out file

        # check if tweets comes from same .out file
        if name_of_file_no_number != stored_name_of_file:  # reset counter for unique identification of file name
            number = 1  # set counter back to starting number 1

        # print("Tweet number:", number, '\n')  # print number on screen
        print('Tweet {0}:\n{1}\n'.format(number, tweet[0]))  # print tweet (first element of tuple) on screen

        # classify tweet for gold label, p = positive, n = negative, <ENTER> = unclear sentiment
        # shows amount of positive and negative tweets collected so far
        sentiment = input("\033[34m<CLASSIFICATION> \033[0mEnter p for \033[32mpositive {0} \033[0mtweet, "
            "n for \033[31mnegative {1} \033[0mtweet or press <ENTER> for unclear {2} tweet: ".format(
            (find_amount_text_files("data/positive")), (find_amount_text_files("data/negative")), (find_amount_text_files("data/unclear"))))  # enter your evaluation

        # classified as positive tweet by user input
        if sentiment == "p":  # save tweet and put in positive folder
            save_path = '/Users/Mike/Desktop/thesis_programs/data/positive'
            save_path_tokenize = '/Users/Mike/Desktop/thesis_programs/data/positive/tokenized'
            completeName = os.path.join(save_path, name_of_file+".txt")
            file = open(completeName, "w")
            file.write(tweet[0])
            file.close()

            # save tweet as tokenized tweet
            completeName = os.path.join(save_path_tokenize, name_of_file+"-tokenized.txt")
            file = open(completeName, "w")
            file.write(str(tknzr.tokenize(tweet[0])))  # write tokenized tweet into tweet'-tokenized' file with the same number
            file.close()

        # classified as negative tweet by user input
        elif sentiment == "n":  # save tweet and put in negative folder
            save_path = '/Users/Mike/Desktop/thesis_programs/data/negative'
            save_path_tokenize = '/Users/Mike/Desktop/thesis_programs/data/negative/tokenized'
            completeName = os.path.join(save_path, name_of_file+".txt")
            file = open(completeName, "w")
            file.write(tweet[0])  # write tweet in lowercase into file
            file.close()

            # save tweet as tokenized tweet
            completeName = os.path.join(save_path_tokenize, name_of_file+"-tokenized.txt")
            file = open(completeName, "w")
            file.write(str(tknzr.tokenize(tweet[0])))  # write tokenized tweet into tweet'-tokenized' file with the same number
            file.close()

        # classified as unclear tweet by user input
        else:
            save_path = '/Users/Mike/Desktop/thesis_programs/data/unclear'
            save_path_tokenize = '/Users/Mike/Desktop/thesis_programs/data/unclear/tokenized'
            completeName = os.path.join(save_path, name_of_file+".txt")
            file = open(completeName, "w")
            file.write(tweet[0])  # write tweet in lowercase into file
            # file.write(user)  # remove the comment to store the username as well
            file.close()

            # save tweet as tokenized tweet
            completeName = os.path.join(save_path_tokenize, name_of_file+"-tokenized.txt")
            file = open(completeName, "w")
            file.write(str(tknzr.tokenize(tweet[0])))  # write tokenized tweet into tweet'-tokenized' file with the same number
            file.close()

        print("\n---------------------------\n")  # print seperator between tweets for more overview
        number += 1  # increase number for tweet identification
        stored_name_of_file = str(tweet[1][5:-4]) + "-tweet-"  # store name for check if tweets comes from same .out file


def collect_tweets(directory):
    """ This function reads JSON .out files in specific directory
        and returns the tweets which contain the given keywords from the files """
    tweets = []  # create empty list for storing gathered tweets
    tweet_tuples = []
    count = 0  # counter for amount of tweets gathered

    # tweet filter by official hashtag
    filterKeyword = \
        ['#tvoh']  # filter tweets by official hashtag

#    *** optional filter setup ***
#     # tweet filter by official hashtag and custom keywords
#    filterKeyword = \
#        ['#tvoh', '#tvoh2017', '#Thevoiceofholland', '#2017tvoh',
#            '@alibouali', '@missmontreal', '@guusmeeuwis', '@waylonline',
#            '@pleunbierbooms', '@vinchenzot', '@isabelprovoost', '@thijspot',
#            '#vinchenzo', '#pleun', '#isabel', '#thijs']  # filter tweets by listed keywords

    for file in find_out_files(directory):
        print("Reading file:", file)
        for line in open(file, 'r'):
            line = line.lower()  # turn tweet into lower case

            for key in filterKeyword:
                if key in json.loads(line)['text']:
                    tweet_name = file.split("/")[-1]
                    tuple = ((json.loads(line)['text']), tweet_name)
                    tweets.append((json.loads(line)['text']))
                    tweet_tuples.append(tuple)
                    count = count + 1  # add number

    print("Amount of tweets collected:", count)
    input("press <ENTER> to continue")
    return tweet_tuples  # return tweets


def combine_files(directory, output):
    """ This function combines text files stored in specific directory
        into one text file """
    filenames = find_text_files(directory)  # collect filenames of tweets
    with open(output, 'w') as outfile:
        for fname in filenames:
            with open(fname) as infile:
                tweet = infile.read()
                #tweet = tweet.replace('\n', '')  # remove new line characters
                outfile.write(tweet+"\n")

def combine_all_positive_negative(directory, output):
    filenames = find_text_files(directory)  # collect filenames of tweets
    with open(output, 'w') as outfile:
        for fname in filenames:
            with open(fname) as infile:
                tweet = infile.read()
                #tweet = tweet.replace('\n', '')  # remove new line characters
                outfile.write(tweet+"\n")

def clean_tweet(directory):
    """ This function cleans tweet files stored in specific directory.
        It will remove emoticons, reduce sequence of characters, remove url's
        and replaces new line characters with spaces. """
    filenames = find_text_files(directory)  # collect filenames of tweets
    regexp = re.compile(r"([a-zA-Z])\1\1\1*")  # find sequences of more then 3 characters in words
    punctuation = "![]/}()${?.,:'"  # to be removed

    for fname in filenames:
        # open and read tweet
        with open(fname, 'r+') as read:
            tweet = read.read()
            tweet = tweet.replace('\n', '')  # remove new line character
            clean_tweet = ""  # set variable for making clean tweet string

            for word in tweet.split():
                clean = ""
                for char in word:
                    if char in punctuation:
                        clean = clean + ""
                    else:
                        clean = clean + char

                # replace url with <URL> /// remove url
                if (word.startswith('http') or word.startswith('https')):
                    clean = ""  # remove url's
                    clean_tweet = clean_tweet + clean + " "

                # regexp / # reduce sequence of characters of word to a maximum of 2
                match = re.search(regexp, clean)
                if match:
                    clean = ''.join(chain.from_iterable(islice(g, 2) for k, g in groupby(clean)))
                    clean_tweet = clean_tweet + clean + " "

                else:
                    clean_tweet = clean_tweet + clean + " "

                # remove emoticons
                clean_tweet = ''.join(char for char in clean_tweet if char <= '\u2600')  # remove emoticons from tweets

        clean_tweet = clean_tweet.rstrip()  # remove trailing white spaces

        # write clean tweet to the opened tweet
        with open(fname, 'w') as write:
            write.write(clean_tweet)


def label_positive(directory):
    """ This function cleans labels tweets stored in specific directory
        It will rewrite the tweet as a tuple in string format with a negative label
        at the end of the string """
    filenames = find_text_files(directory)  # collect filenames of tweets
    tuple_list = []
    for fname in filenames:
        # open and read tweet
        with open(fname, 'r+') as read:
            tweet = read.read()
            tuple = (tweet, "pos")

        # write tweet + label into the opened tweet
        with open(fname, 'w') as write:
            write.write(str(tuple))


def label_negative(directory):
    """ This function cleans labels tweets stored in specific directory
        It will rewrite the tweet as a tuple in string format with a negative label
        at the end of the string """
    filenames = find_text_files(directory)  # collect filenames of tweets
    tuple_list = []
    for fname in filenames:
        # open and read tweet
        with open(fname, 'r+') as read:
            tweet = read.read()
            tuple = (tweet, "neg")

        # write tweet + label into the opened tweet
        with open(fname, 'w') as write:
            write.write(str(tuple))


def list_tweets_label(directory, label):
    """ This function returns tweets with given label stored in specific directory"""
    tweets_with_labels = []
    filenames = find_text_files(directory)  # collect filenames of tweets
    for fname in filenames:
        with open(fname, 'r+') as read:
            tweet = read.read()
            tuple = (tweet, label)
            tweets_with_labels.extend(tuple)

    return(tweets_with_labels)


def retrieve_tweets(directory):
    """ This function returns tweets stored in specific directory """
    filenames = find_text_files(directory)  # collect filenames of tweets
    tweet_list = []
    for fname in filenames:
        with open(fname, 'r+') as read:
            tweet = read.read()
            tweet_list.append(tweet)

    return(tweet_list)


def save_collected_tweets(tweet_tuples, save_path):
    """ This function reads a list of tuples with tweets and
        their file names and returns the (pre-pre processed) tweets of
        the files which then could be classified into positive
        or negative by user input and stores the annotated tweets
        into seperate corresponding directory with unique file name for
        identification / traceablilty """

    number = 1  # set starting number for tweet identification
    tknzr = TweetTokenizer()  # construct tokenizer for tweets
    stored_name_of_file = ""  # create variable for check if tweets comes from same .out file

    for tweet in tweet_tuples:  # loop trought each tweet in JSON .out file

        # set name of tweet file
        name_of_file = str(tweet[1][:-4]) + "-tweet-" + str(number)  # set name of file as tweet + number for unique identification
        name_of_file_no_number = str(tweet[1][:-4]) + "-tweet-"  # store name for check if tweets comes from same .out file

        # check if tweets comes from same .out file
        if name_of_file_no_number != stored_name_of_file:  # reset counter for unique identification of file name
            number = 1  # set counter back to starting number 1

        completeName = save_path + name_of_file + ".txt"
        file = open(completeName, "w")
        file.write(tweet[0])  # write tweet in lowercase into file
        file.close()

        number += 1  # increase number for tweet identification
        stored_name_of_file = str(tweet[1][:-4]) + "-tweet-"  # store name for check if tweets comes from same .out file
