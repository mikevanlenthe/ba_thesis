# Functions
# Thesis functions (pre-processing)
from thesis_functions import *
from machine_learning_functions import *
# NLTK
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import subjectivity
from nltk.sentiment import SentimentAnalyzer
from nltk.sentiment.util import *
# classifiers
from nltk.classify import NaiveBayesClassifier
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from nltk.classify import ClassifierI
# metrics
from nltk.metrics import *
from statistics import mode
from sklearn.metrics import classification_report, confusion_matrix
# random
import random
from operator import itemgetter
import collections
# table
import pyexcel as pe
