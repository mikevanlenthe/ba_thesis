from machine_learning import *

classifier = nltk.NaiveBayesClassifier.train(training_set)


######################################################
# testing prediction
print("######################################################")
print("Predictions:")
# prediction of possible winner on development + test set by trained NaiveBayesClassifier
print("Predictions on gold data set (dev+test) by NaiveBayesClassifier:")
# collect sentiment score for each candidate *** modulair by list entry ***
candidates = ["thijs", "pleun", "vinchenzo", "isabel"]
jury = ["ali", "waylon", "guus", "sanne"]
sentiment_scores = []

for candidate in candidates:
    positive_counter, negative_counter, tweet_count = 0, 0, 0  # setup counters
    for tweet in testing_tweets:  # set to unseen_tweets to predict the next round
        if candidate in tweet[0]:
            prediction = classifier.prob_classify(extract_features(tweet[0]))
            # sentiment rating for tweet must be atleast 70% positive
            if prediction.max() == "positive" and prediction.prob("positive") >= .9:
                # increase counters by 1 if positive tweet found about candidate
                positive_counter += 1
                tweet_count += 1

            # sentiment rating for tweet must be atleast 70% negative
            if prediction.max() == "negative" and prediction.prob("negative") >= .9:
                # increase counters by 1 if negative tweet found about candidate
                negative_counter += 1
                tweet_count += 1

    sentiment_score = positive_counter - negative_counter
    sentiment_percentage = round((sentiment_score / tweet_count) * 100, 1)

    tuple = (
        candidate, tweet_count, positive_counter, negative_counter, sentiment_score,
         sentiment_percentage
        )  # set results of candidate into tuple format

    # append candidate scores to sentiment_scores list
    sentiment_scores.append(tuple)

print("On score:")
sentiment_scores.sort(key=itemgetter(4), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {0}: totaal aantal tweets: {1}, aantal positief: {2}, "
        "aantal negatief: {3}, sentiment score: {4}, percentage: {5}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

sheet = pe.Sheet(sentiment_scores)
sheet.colnames = "Naam", "Totaal", "Positief", "Negatief", "Sent-score", "Sent-percentage"
print(sheet)

print()
print("On percentage:")
sentiment_scores.sort(key=itemgetter(5), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {0}: totaal aantal tweets: {1}, aantal positief: {2}, "
        "aantal negatief: {3}, sentiment score: {4}, percentage: {5}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

sheet = pe.Sheet(sentiment_scores)
sheet.colnames = "Naam", "Totaal", "Positief", "Negatief", "S-score", "S-percentage"
print(sheet)

######################################################

print()

print("Testing prediction on string:")
test_pos_string = "toch nog heel mooi gezongen!"
test_neg_string = "dat was misschien wel erg vals gezongen!"

print("Test positive sentence:", test_pos_string, "classified as:",
    classifier.classify(extract_features(tknzr.tokenize(test_pos_string))))

print("Test negative sentence:", test_neg_string, "classified as:",
    classifier.classify(extract_features(tknzr.tokenize(test_neg_string))))

print()
