from thesis_functions import *
import os

def main():
    # setup directory where to change the file names
    # source = '/Users/Mike/Desktop/thesis_programs/data/unclear/tokenized/'

    files = os.listdir(source)
    new_filename = "2017021721-tweet-" # new file name
    for filename in files:
        print(filename) # print old file name
        if filename.startswith("tweet"):
            os.rename(source+filename, source+new_filename+filename[5:]) # slice first 5 characters out


if __name__ == '__main__':
    main()
