from all_imports import *


def extract_features(document):
    """ This function checks if the word features from specific documents
    exists in training tweets """
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features


# set all tweets in lists
positive_tweets_list = \
    retrieve_tweets('data/positive')  # create positive tweet list
negative_tweets_list = \
    retrieve_tweets('data/negative')  # create negative tweet list

# constructing
tknzr = TweetTokenizer()  # create  special tokenizer for twitter messages
n_instances = 450  # amount of tweets = x 2

# select random tweets with random.seed()
# and random.sample() for reproducibility
random.seed(n_instances)

# make split points to divide data into train, development and test
split_point = int(0.70 * n_instances)  # 70%
split_point2 = int(0.30 * n_instances // 3)  # 10%
split_point3 = split_point + split_point2  # 20%

######################################################
# collecting data for machine learning data set from tweet lists
# use n_instances of positive tweets
positive_tweets = \
    [(tknzr.tokenize(tweet), 'positive')
        for tweet in random.sample(positive_tweets_list, n_instances)]

# use n_instances of negative tweets
negative_tweets = \
    [(tknzr.tokenize(tweet), 'negative')
        for tweet in random.sample(negative_tweets_list, n_instances)]

print("Data collection for Machine Learning:")
print("Amount of tweets in use:",
    (len(positive_tweets)+len(negative_tweets)))  # print amount of tweets used in total

######################################################
# split data in: train data, 70% of total data,
training_tweets = \
    positive_tweets[:split_point] + negative_tweets[:split_point]
print("Amount of tweets in training set:",
    len(training_tweets))  # print amount of training tweets

# development data, 10% of total data,
development_tweets = \
    positive_tweets[split_point:split_point3:] + negative_tweets[split_point:split_point3:]
print("Amount of tweets in development set:",
    len(development_tweets))  # print amount of development tweets

# test data, 20% of total data,
testing_tweets = \
    positive_tweets[split_point3:] + negative_tweets[split_point3:]
print("Amount of tweets in test set:",
    len(testing_tweets))  # print amount of test tweets

# combining development & test data # 30% of the data
dev_and_test_tweets = \
    development_tweets + testing_tweets  # 10% + 20%
print("Amount of tweets in development & test set combined:",
    len(dev_and_test_tweets))  # print amount of test tweets

######################################################
print("Features in use: unigram")
# word features on training_tweets
# uni-gram
word_features = \
    filter_feature_words(get_word_features(unigram_in_tweets(training_tweets)))

# uni-gram + bi-gram
# word_features = \
#    filter_feature_words(get_word_features(unigram_in_tweets(training_tweets) +
#    bigram_in_tweets(training_tweets)))

# uni-gram + bi-gram + tri-gram
# word_features = \
#    filter_feature_words(get_word_features(unigram_in_tweets(training_tweets) +
#    bigram_in_tweets(training_tweets) +
#    trigram_in_tweets(training_tweets)))

# bi-gram
# word_features = \
#    filter_feature_words(get_word_features(bigram_in_tweets(training_tweets)))

# bi-gram + tri-gram
# word_features = \
#    filter_feature_words(get_word_features(bigram_in_tweets(training_tweets) +
#    trigram_in_tweets(training_tweets)))

# tri-gram
# word_features = \
#    filter_feature_words(get_word_features(trigram_in_tweets(training_tweets)))

# apply features on data sets
training_set = \
    nltk.classify.apply_features(extract_features, training_tweets)

development_set = \
    nltk.classify.apply_features(extract_features, development_tweets)

test_set = \
    nltk.classify.apply_features(extract_features, testing_tweets)

# dev_and_test_set = \
#     nltk.classify.apply_features(extract_features, dev_and_test_tweets)
#
# positive_set = \
#     nltk.classify.apply_features(extract_features, positive_tweets)
#
# negative_set = \
#     nltk.classify.apply_features(extract_features, negative_tweets)

print()
