from machine_learning import *
from machine_learning_other_classifiers import *


print("Predictions on liveshow 1 data by VotingClassifier:")
# data collection of unseen tweets
unseen_tweets = \
    retrieve_tweets('data/liveshow1')

# collect sentiment score for each candidate *** modulair by list entry ***
candidates = ["leon", "katell", "thijs", "vinchenzo", "dwight", "roza", "pleun", "romy", "yerry", "kirsten", "isabel", "sheela"]
jurys = ["ali", "waylon", "guus", "sanne"]
sentiment_scores = []

for candidate in candidates:
    positive_counter, negative_counter, tweet_count = 0, 0, 0  # setup counters
    for tweet in unseen_tweets:  # set to unseen_tweets to predict the next round
        if candidate in tweet:
            prediction = voted_classifier.classify(extract_features(tknzr.tokenize(tweet)))
            confidence = voted_classifier.confidence(extract_features(tknzr.tokenize(tweet)))
            # sentiment rating from voting system for tweet must be atleast 70% agreement on positive
            if prediction == "positive" and confidence >= .7:
                # increase counters by 1 if positive tweet found about candidate
                positive_counter += 1
                tweet_count += 1

            # sentiment rating from voting system for tweet must be atleast 70% agreement on negative
            if prediction == "negative" and confidence >= .7:
                # increase counters by 1 if negative tweet found about candidate
                negative_counter += 1
                tweet_count += 1

    sentiment_score = positive_counter - negative_counter
    sentiment_percentage = round((sentiment_score / tweet_count) * 100, 1)

    tuple = (
        positive_counter, negative_counter, sentiment_score,
        candidate, sentiment_percentage, tweet_count
        )  # set results of candidate into tuple format

    # append candidate scores to sentiment_scores list
    sentiment_scores.append(tuple)

print("On score:")
sentiment_scores.sort(key=itemgetter(2), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
print("On percentage:")
sentiment_scores.sort(key=itemgetter(4), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()

######################################################
print("Predictions on liveshow 2 data by VotingClassifier:")
# data collection of unseen tweets
unseen_tweets = \
    retrieve_tweets('data/liveshow2')

# collect sentiment score for each candidate *** modulair by list entry ***
candidates = ["leon", "katell", "thijs", "vinchenzo", "dwight", "pleun", "romy", "yerry", "kirsten", "isabel"]
jurys = ["ali", "waylon", "guus", "sanne"]
sentiment_scores = []

for candidate in candidates:
    positive_counter, negative_counter, tweet_count = 0, 0, 0  # setup counters
    for tweet in unseen_tweets:  # set to unseen_tweets to predict the next round
        if candidate in tweet:
            prediction = voted_classifier.classify(extract_features(tknzr.tokenize(tweet)))
            confidence = voted_classifier.confidence(extract_features(tknzr.tokenize(tweet)))
            # sentiment rating from voting system for tweet must be atleast 70% agreement on positive
            if prediction == "positive" and confidence >= .7:
                # increase counters by 1 if positive tweet found about candidate
                positive_counter += 1
                tweet_count += 1

            # sentiment rating from voting system for tweet must be atleast 70% agreement on negative
            if prediction == "negative" and confidence >= .7:
                # increase counters by 1 if negative tweet found about candidate
                negative_counter += 1
                tweet_count += 1

    sentiment_score = positive_counter - negative_counter
    sentiment_percentage = round((sentiment_score / tweet_count) * 100, 1)

    tuple = (
        positive_counter, negative_counter, sentiment_score,
        candidate, sentiment_percentage, tweet_count
        )  # set results of candidate into tuple format

    # append candidate scores to sentiment_scores list
    sentiment_scores.append(tuple)

print("On score:")
sentiment_scores.sort(key=itemgetter(2), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
print("On percentage:")
sentiment_scores.sort(key=itemgetter(4), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
######################################################
print("Predictions on liveshow 3 data by VotingClassifier:")
# data collection of unseen tweets
unseen_tweets = \
    retrieve_tweets('data/liveshow3')

# collect sentiment score for each candidate *** modulair by list entry ***
candidates = ["leon", "thijs", "vinchenzo", "dwight", "pleun", "yerry", "kirsten", "isabel"]
jurys = ["ali", "waylon", "guus", "sanne"]
sentiment_scores = []

for candidate in candidates:
    positive_counter, negative_counter, tweet_count = 0, 0, 0  # setup counters
    for tweet in unseen_tweets:  # set to unseen_tweets to predict the next round
        if candidate in tweet:
            prediction = voted_classifier.classify(extract_features(tknzr.tokenize(tweet)))
            confidence = voted_classifier.confidence(extract_features(tknzr.tokenize(tweet)))
            # sentiment rating from voting system for tweet must be atleast 70% agreement on positive
            if prediction == "positive" and confidence >= .7:
                # increase counters by 1 if positive tweet found about candidate
                positive_counter += 1
                tweet_count += 1

            # sentiment rating from voting system for tweet must be atleast 70% agreement on negative
            if prediction == "negative" and confidence >= .7:
                # increase counters by 1 if negative tweet found about candidate
                negative_counter += 1
                tweet_count += 1

    sentiment_score = positive_counter - negative_counter
    sentiment_percentage = round((sentiment_score / tweet_count) * 100, 1)

    tuple = (
        positive_counter, negative_counter, sentiment_score,
        candidate, sentiment_percentage, tweet_count
        )  # set results of candidate into tuple format

    # append candidate scores to sentiment_scores list
    sentiment_scores.append(tuple)

print("On score:")
sentiment_scores.sort(key=itemgetter(2), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
print("On percentage:")
sentiment_scores.sort(key=itemgetter(4), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
######################################################
print("Predictions on liveshow 4 data by VotingClassifier:")
# data collection of unseen tweets
unseen_tweets = \
    retrieve_tweets('data/liveshow4')

# collect sentiment score for each candidate *** modulair by list entry ***
candidates = ["leon", "thijs", "vinchenzo", "dwight", "pleun", "yerry", "isabel"]
jurys = ["ali", "waylon", "guus", "sanne"]
sentiment_scores = []

for candidate in candidates:
    positive_counter, negative_counter, tweet_count = 0, 0, 0  # setup counters
    for tweet in unseen_tweets:  # set to unseen_tweets to predict the next round
        if candidate in tweet:
            prediction = voted_classifier.classify(extract_features(tknzr.tokenize(tweet)))
            confidence = voted_classifier.confidence(extract_features(tknzr.tokenize(tweet)))
            # sentiment rating from voting system for tweet must be atleast 70% agreement on positive
            if prediction == "positive" and confidence >= .7:
                # increase counters by 1 if positive tweet found about candidate
                positive_counter += 1
                tweet_count += 1

            # sentiment rating from voting system for tweet must be atleast 70% agreement on negative
            if prediction == "negative" and confidence >= .7:
                # increase counters by 1 if negative tweet found about candidate
                negative_counter += 1
                tweet_count += 1

    sentiment_score = positive_counter - negative_counter
    sentiment_percentage = round((sentiment_score / tweet_count) * 100, 1)

    tuple = (
        positive_counter, negative_counter, sentiment_score,
        candidate, sentiment_percentage, tweet_count
        )  # set results of candidate into tuple format

    # append candidate scores to sentiment_scores list
    sentiment_scores.append(tuple)

print("On score:")
sentiment_scores.sort(key=itemgetter(2), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
print("On percentage:")
sentiment_scores.sort(key=itemgetter(4), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
######################################################
print("Predictions on liveshow 5 data by VotingClassifier:")
# data collection of unseen tweets
unseen_tweets = \
    retrieve_tweets('data/liveshow5')

# collect sentiment score for each candidate *** modulair by list entry ***
candidates = ["leon", "thijs", "vinchenzo", "dwight", "pleun", "isabel"]
jurys = ["ali", "waylon", "guus", "sanne"]
sentiment_scores = []

for candidate in candidates:
    positive_counter, negative_counter, tweet_count = 0, 0, 0  # setup counters
    for tweet in unseen_tweets:  # set to unseen_tweets to predict the next round
        if candidate in tweet:
            prediction = voted_classifier.classify(extract_features(tknzr.tokenize(tweet)))
            confidence = voted_classifier.confidence(extract_features(tknzr.tokenize(tweet)))
            # sentiment rating from voting system for tweet must be atleast 70% agreement on positive
            if prediction == "positive" and confidence >= .7:
                # increase counters by 1 if positive tweet found about candidate
                positive_counter += 1
                tweet_count += 1

            # sentiment rating from voting system for tweet must be atleast 70% agreement on negative
            if prediction == "negative" and confidence >= .7:
                # increase counters by 1 if negative tweet found about candidate
                negative_counter += 1
                tweet_count += 1

    sentiment_score = positive_counter - negative_counter
    sentiment_percentage = round((sentiment_score / tweet_count) * 100, 1)

    tuple = (
        positive_counter, negative_counter, sentiment_score,
        candidate, sentiment_percentage, tweet_count
        )  # set results of candidate into tuple format

    # append candidate scores to sentiment_scores list
    sentiment_scores.append(tuple)

print("On score:")
sentiment_scores.sort(key=itemgetter(2), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
print("On percentage:")
sentiment_scores.sort(key=itemgetter(4), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
######################################################
print("Predictions on finale data by VotingClassifier:")
# data collection of unseen tweets
unseen_tweets = \
    retrieve_tweets('data/finale')

# collect sentiment score for each candidate *** modulair by list entry ***
candidates = ["thijs", "pleun", "vinchenzo", "isabel"]
jurys = ["ali", "waylon", "guus", "sanne"]
sentiment_scores = []

for candidate in candidates:
    positive_counter, negative_counter, tweet_count = 0, 0, 0  # setup counters
    for tweet in unseen_tweets:  # set to unseen_tweets to predict the next round
        if candidate in tweet:
            prediction = voted_classifier.classify(extract_features(tknzr.tokenize(tweet)))
            confidence = voted_classifier.confidence(extract_features(tknzr.tokenize(tweet)))
            # sentiment rating from voting system for tweet must be atleast 70% agreement on positive
            if prediction == "positive" and confidence >= .7:
                # increase counters by 1 if positive tweet found about candidate
                positive_counter += 1
                tweet_count += 1

            # sentiment rating from voting system for tweet must be atleast 70% agreement on negative
            if prediction == "negative" and confidence >= .7:
                # increase counters by 1 if negative tweet found about candidate
                negative_counter += 1
                tweet_count += 1

    sentiment_score = positive_counter - negative_counter
    sentiment_percentage = round((sentiment_score / tweet_count) * 100, 1)

    tuple = (
        positive_counter, negative_counter, sentiment_score,
        candidate, sentiment_percentage, tweet_count
        )  # set results of candidate into tuple format

    # append candidate scores to sentiment_scores list
    sentiment_scores.append(tuple)

print("On score:")
sentiment_scores.sort(key=itemgetter(2), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter

print()
print("On percentage:")
sentiment_scores.sort(key=itemgetter(4), reverse=True)
score_counter = 1
for score in sentiment_scores:
    print(
        "Nr: {6}, {3}: totaal aantal tweets: {5}, aantal positief: {0}, "
        "aantal negatief: {1}, sentiment score: {2}, percentage: {4}%"
        .format(score[0], score[1], score[2], score[3], score[4], score[5], score_counter))

    score_counter += 1  # increase score_counter
