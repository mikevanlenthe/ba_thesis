from all_imports import *
import nltk


def unigram_in_tweets(tweet):
    """ This function returns unigrams from a tweet """
    unigrams = []
    for (unigram, sentiment) in tweet:
        unigrams.extend(unigram)
    return unigrams


def bigram_in_tweets(tweets):
    """ This function returns bigrams from a tweet """
    bigrams = \
        [" ".join(bigram) for tweet, sentiment in tweets
            for bigram in nltk.bigrams(tweet)]
    return bigrams


def trigram_in_tweets(tweets):
    """ This function returns trigrams from a tweet """
    trigrams = \
        [" ".join(trigram) for tweet, sentiment in tweets
            for trigram in nltk.trigrams(tweet)]
    return trigrams


def get_word_features(wordlist):
    """ This function returns the word features (most common) from
        a wordlist (for example unigrams of development tweets)"""
    wordlist = nltk.FreqDist(wordlist)
    word_features = wordlist.keys()
    return word_features


def filter_feature_words(words):
    """ This function filters out uninformative word features """
    uninformative = set(('broek', 'ronnie', 'flex', 'thijs', 'pleun', 'isabel', 'vinchenzo', '#pleun', '@pleunbierbooms',
        '#isabel', 'sanne', 'duet', '@waylonline', '#vinchenzo', 'jamai', '#thijs', '*', 'pascal', '@vinchenzot',
        'lieve', '&', 'wendy', '"', '@rtl_tvoh', 'guus', 'vincenzo', 'waylon', '@arminvanbuuren', '2', '@missmontreal',
        '#ronnieflex', 'martijn', '@fockeline', 'rt', '#rtl4', '#finale', '3', 'meeuwis'))
    return dict([(word, True) for word in words if word not in uninformative])
