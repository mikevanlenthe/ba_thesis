from thesis_functions import *


def main():
    #pre-process tweets (clean)
    clean_tweet("data/positive")
    clean_tweet("data/negative")
    clean_tweet("data/unclear")
    clean_tweet("data/liveshow1")
    clean_tweet("data/liveshow2")
    clean_tweet("data/liveshow3")
    clean_tweet("data/liveshow4")
    clean_tweet("data/liveshow5")
    clean_tweet("data/finale")

if __name__ == '__main__':
    main()
